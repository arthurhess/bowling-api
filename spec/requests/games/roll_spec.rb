# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'POST /games/:id/roll' do
  it 'updates the rolls of the game' do
    game = Game.create(rolls: [8])

    post roll_game_path(game), params: { score: 2 }

    expect(game.reload.rolls).to eq [8, 2]
  end

  it 'returns the updated game' do
    game = Game.create(rolls: [8])

    post roll_game_path(game), params: { score: 2 }

    parsed_body = JSON.parse(response.body, { symbolize_names: true })
    expect(parsed_body).to eq(
      {
        id: game.id,
        frames: [{ rolls: [8, 2], score: 10 }],
        score: 10
      }
    )
  end

  it 'returns 200' do
    game = Game.create(rolls: [8])

    post roll_game_path(game), params: { score: 2 }

    expect(response).to have_http_status :ok
  end
end
