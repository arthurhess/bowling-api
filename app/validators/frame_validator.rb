# frozen_string_literal: true

class FrameValidator < ActiveModel::Validator
  def validate(record)
    rolls_are_between_zero_and_ten(record)

    regular_spare_frame_has_two_rolls(record)
    last_spare_frame_has_at_most_three_rolls(record)

    regular_strike_frame_has_one_roll(record)
    last_strike_frame_has_at_most_three_rolls(record)

    regular_frame_has_at_most_two_rolls(record)
    regular_frame_rolls_sum_up_to_at_most_ten(record)
    last_frame_rolls_sum_up_to_at_most_thirty(record)
  end

  private

  def regular_strike_frame_has_one_roll(record)
    return unless record.strike?
    return if record.last?
    return if record.rolls.size == 1

    record.errors.add(:rolls, 'are invalid')
  end

  def last_strike_frame_has_at_most_three_rolls(record)
    return unless record.strike? && record.last?
    return if record.rolls.size <= 3

    record.errors.add(:rolls, 'are too many')
  end

  def regular_spare_frame_has_two_rolls(record)
    return unless record.spare?
    return if record.last?
    return if record.rolls.size == 2

    record.errors.add(:rolls, 'are invalid')
  end

  def last_spare_frame_has_at_most_three_rolls(record)
    return unless record.spare? && record.last?
    return if record.rolls.size <= 3

    record.errors.add(:rolls, 'are too many')
  end

  def regular_frame_has_at_most_two_rolls(record)
    return if record.strike? || record.spare?
    return if record.rolls.size <= 2

    record.errors.add(:rolls, 'are too many')
  end

  def regular_frame_rolls_sum_up_to_at_most_ten(record)
    return if record.last?
    return if record.rolls.sum <= 10

    record.errors.add(:rolls, 'sum are more than ten in a regular frame')
  end

  def last_frame_rolls_sum_up_to_at_most_thirty(record)
    return if record.last?
    return if record.rolls.sum <= 30

    record.errors.add(:rolls, 'sum are more than thirty in the last frame')
  end

  def rolls_are_between_zero_and_ten(record)
    return if record.rolls.all? { |roll| roll.between?(0, 10) }

    record.errors.add(:rolls, 'are not all between zero and ten')
  end
end
