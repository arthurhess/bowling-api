# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ParseFrames do
  describe '#call' do
    it 'returns an array of Frames' do
      rolls = [4, 8, 10, 5, 5]

      result = described_class.new(rolls).call

      expect(result).to all(be_a(Frame))
    end

    it 'parses frames correctly' do
      rolls = [4, 8, 10, 5, 5]

      result = described_class.new(rolls).call

      expect(result).to eq [
        Frame.new(rolls: [4, 8], score: 12, index: 1),
        Frame.new(rolls: [10], score: 20, index: 2),
        Frame.new(rolls: [5, 5], score: 10, index: 3)
      ]
    end
  end
end
