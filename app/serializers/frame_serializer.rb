# frozen_string_literal: true

class FrameSerializer < ActiveModel::Serializer
  attributes :rolls, :score
end
