# frozen_string_literal: true

require 'rails_helper'

RSpec.describe FrameSerializer do
  it 'serializes single frame' do
    frame = Frame.new(rolls: [5, 5], index: 1, score: 10)

    result = ActiveModelSerializers::SerializableResource.new(frame).as_json

    expect(result).to eq(
      { rolls: [5, 5], score: 10 }
    )
  end

  it 'serializes a collection of frames' do
    frame1 = Frame.new(rolls: [5, 5], index: 1, score: 20)
    frame2 = Frame.new(rolls: [8, 2], index: 2, score: 10)
    frames = [frame1, frame2]

    result = ActiveModelSerializers::SerializableResource.new(frames).as_json

    expect(result).to eq(
      [
        { rolls: [5, 5], score: 20 },
        { rolls: [8, 2], score: 10 }
      ]
    )
  end
end
