# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'GET /games/:id' do
  context 'when game exists' do
    it 'returns 200' do
      game = Game.create(rolls: [5, 5])

      get game_path(game)

      expect(response).to have_http_status :ok
    end

    it 'returns serialized game' do
      game = Game.create(rolls: [5, 5])

      get game_path(game)

      parsed_body = JSON.parse(response.body, { symbolize_names: true })
      expect(parsed_body).to eq(
        {
          id: game.id,
          score: 10,
          frames: [
            { rolls: [5, 5], score: 10 }
          ]
        }
      )
    end
  end

  context 'when game does not exist' do
    it 'returns 404' do
      get game_path('junk')

      expect(response).to have_http_status :not_found
    end
  end
end
