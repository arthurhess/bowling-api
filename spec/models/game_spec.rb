# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Game do
  describe '#frames' do
    it 'returns an array of Frame objects' do
      rolls = [8, 2, 10]
      game = Game.new(rolls: rolls)

      expect(game.frames).to all(be_a(Frame))
    end
  end
end
