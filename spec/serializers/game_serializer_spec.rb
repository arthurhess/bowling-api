# frozen_string_literal: true

require 'rails_helper'

RSpec.describe GameSerializer do
  it 'serializes a game' do
    game = Game.create(rolls: [8, 2, 10, 8])

    result = ActiveModelSerializers::SerializableResource.new(game).as_json

    expect(result).to eq(
      {
        id: game.id,
        frames: [
          { rolls: [8, 2], score: 20 },
          { rolls: [10], score: 18 },
          { rolls: [8], score: 8 }
        ],
        score: 46
      }
    )
  end
end
