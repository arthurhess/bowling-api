# frozen_string_literal: true

class Frame < OpenStruct
  include ActiveModel::Validations
  include ActiveModel::Serialization

  validates_with FrameValidator

  def last?
    index == 10
  end

  def spare?
    !strike? && rolls[0..1].sum == 10
  end

  def strike?
    rolls.first == 10
  end
end
