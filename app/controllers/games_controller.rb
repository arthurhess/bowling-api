# frozen_string_literal: true

class GamesController < ApplicationController
  def create
    render json: Game.create!, status: :created
  end

  def roll
    result = SaveRoll.call(game: game, roll: roll_score)

    if result.success?
      render json: result.game
    else
      head :unprocessable_entity
    end
  end

  def show
    render json: game
  end

  private

  def game
    Game.find(params[:id])
  end

  def roll_score
    params.require(:score).to_i
  end
end
