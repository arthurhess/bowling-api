# frozen_string_literal: true

class Game < ApplicationRecord
  validate :all_frames_are_valid

  def frames
    ParseFrames.new(rolls).call
  end

  def score
    frames.sum { |frame| frame[:score] }
  end

  private

  def all_frames_are_valid
    return if frames.all?(&:valid?)

    errors.add(:frames, 'are not all valid')
  end
end
