# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'POST /games' do
  it 'creates a game' do
    expect { post games_path }.to change { Game.count }.by(1)
  end

  it 'returns the created game' do
    post games_path

    parsed_body = JSON.parse(response.body, { symbolize_names: true })
    expect(parsed_body).to match(
      {
        id: a_kind_of(Integer),
        frames: [],
        score: 0
      }
    )
  end

  it 'returns 201' do
    post games_path

    expect(response).to have_http_status :created
  end
end
