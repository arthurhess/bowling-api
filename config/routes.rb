# frozen_string_literal: true

Rails.application.routes.draw do
  root to: redirect('/docs')

  resources :games, only: %i[create show] do
    post 'roll', to: 'games#roll', on: :member
  end
end
