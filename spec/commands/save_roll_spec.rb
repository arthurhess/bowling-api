# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SaveRoll do
  describe '#call' do
    context 'when roll is valid' do
      it 'returns success' do
        game = Game.create(rolls: [8])

        result = described_class.call(game: game, roll: 2)

        expect(result).to be_success
      end

      it 'saves roll' do
        game = Game.create(rolls: [8])

        described_class.call(game: game, roll: 2)

        expect(game.reload.rolls).to eq [8, 2]
      end
    end

    context 'when roll is invalid' do
      it 'returns failure' do
        game = Game.create(rolls: [8])

        result = described_class.call(game: game, roll: 3)

        expect(result).to be_failure
      end

      it 'does not save roll' do
        game = Game.create(rolls: [8])

        described_class.call(game: game, roll: 3)

        expect(game.reload.rolls).to eq [8]
      end
    end
  end
end
