# frozen_string_literal: true

class ParseFrames
  def initialize(rolls)
    @rolls = rolls
  end

  # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
  def call
    frames = []
    index = 0

    while @rolls[index].present? && frames.size <= 10
      last_frame = frames.size == 9

      if strike?(index)
        frames << Frame.new(
          rolls: last_frame ? @rolls[index..index + 2] : [10],
          score: @rolls[index..index + 2].sum,
          index: frames.size + 1
        )

        index += 1
      elsif spare?(index)
        frames << Frame.new(
          rolls: last_frame ? @rolls[index..index + 2] : @rolls[index..index + 1],
          score: @rolls[index..index + 2].sum,
          index: frames.size + 1
        )

        index += 2
      else
        frames << Frame.new(
          rolls: @rolls[index..index + 1],
          score: @rolls[index..index + 1].sum,
          index: frames.size + 1
        )

        index += 2
      end
    end

    frames
  end
  # rubocop:enable Metrics/AbcSize, Metrics/MethodLength

  private

  def strike?(roll)
    @rolls[roll] == 10
  end

  def spare?(roll)
    !strike?(roll) && @rolls[roll..roll + 1].sum == 10
  end
end
