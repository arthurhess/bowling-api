# Bowling API

- [Bowling API](#bowling-api)
    - [Installation](#installation)
        - [Running with Docker Compose](#running-with-docker-compose)
        - [Deploying to Heroku](#deploying-to-heroku)
    - [API documentation](#api-documentation)
    - [Demo](#demo)
        - [Starting a game](#starting-a-game)
        - [Saving a score](#saving-a-score)
        - [Retrieving a game](#retrieving-a-game)

---
### Installation

##### Running with Docker Compose

Clone this repository:

```bash
$ git clone git@bitbucket.org:arthurhess/bowling-api.git
$ cd bowling-api/
```

Build the services:

```bash
$ docker-compose build
```

Setup the database:

```bash
$ docker-compose run bowling-api bin/rails db:setup
```

Run the tests:

```bash
$ docker-compose run bowling-api bin/rspec
```

Start the services:

```bash
$ docker-compose up
```

Afterwards, the API server should be available at http://localhost:3001/

##### Deploying to Heroku

Create an app and feed the master branch to it:

```bash
$ heroku apps:create
$ git push heroku master
```

View it in your browser:

```bash
$ heroku open
```

### API documentation

A Swagger-generated documentation is available under [/docs](https://bowling-api-arthurhess.herokuapp.com/docs).
### Demo

This app is hosted [on Heroku](https://bowling-api-arthurhess.herokuapp.com/).

##### Starting a game

```bash
$ curl -X POST https://bowling-api-arthurhess.herokuapp.com/games

{"id":1,"score":0,"frames":[]}
```

##### Saving a score
```bash
$ curl -X POST -H "Content-Type: application/json" -d '{"score": 8}' https://bowling-api-arthurhess.herokuapp.com/games/1/roll

{"id":1,"score":8,"frames":[{"rolls":[8],"score":8}]}
```

##### Retrieving a game
```bash
$ curl -X GET https://bowling-api-arthurhess.herokuapp.com/games/1

{"id":1,"score":8,"frames":[{"rolls":[8],"score":8}]}
```