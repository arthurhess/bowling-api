# frozen_string_literal: true

class SaveRoll
  include Interactor

  def call
    context.game.update(
      rolls: context.game.rolls << context.roll
    )

    if context.game.invalid?
      context.game.reload
      context.fail!
    end
  end
end
