# frozen_string_literal: true

class GameSerializer < ActiveModel::Serializer
  attributes :id, :score
  has_many :frames
end
