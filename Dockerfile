FROM ruby:3.0.0
RUN apt-get update && apt-get install -y postgresql-client
RUN mkdir /bowling-api
WORKDIR /bowling-api
COPY . /bowling-api

RUN bundle install
