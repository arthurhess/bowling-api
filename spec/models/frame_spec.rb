# frozen_string_literal: true

require 'rails_helper'

# To the reviewer:
#
# I do realize my specs are a bit verbose
# as I chose not to use too much of RSpec's before/let/subject.
#
# I usually prefer being verbose in my specs,
# following an 'Arrange, Act, Assert' pattern.
#
# Of course I always try to fit my style to match that of
# the team I'm working with.
#
RSpec.describe Frame do
  let(:default_params) do
    {
      rolls: [],
      index: 1
    }
  end

  describe '#last?' do
    it 'returns true when frame has the highest index' do
      params = default_params.merge(index: 10)
      frame = Frame.new(params)

      expect(frame.last?).to eq true
    end

    it 'returns false otherwise' do
      params = default_params.merge(index: 9)
      frame = Frame.new(params)

      expect(frame.last?).to eq false
    end
  end

  describe '#spare?' do
    it 'returns true when frame has two rows summing up to 10' do
      params = default_params.merge(rolls: [8, 2])
      frame = Frame.new(params)

      expect(frame.spare?).to eq true
    end

    it 'returns false otherwise' do
      params = default_params.merge(rolls: [6, 2])
      frame = Frame.new(params)

      expect(frame.spare?).to eq false
    end
  end

  describe '#strike?' do
    it 'returns true when first roll is 10' do
      params = default_params.merge(rolls: [10])
      frame = Frame.new(params)

      expect(frame.strike?).to eq true
    end

    it 'returns false otherwise' do
      params = default_params.merge(rolls: [6, 2])
      frame = Frame.new(params)

      expect(frame.strike?).to eq false
    end
  end

  describe '#valid?' do
    it 'returns false when rolls are invalid' do
      params = default_params.merge(rolls: [6, 5])
      frame = Frame.new(params)

      expect(frame).not_to be_valid
    end

    it 'returns false when regular frame has too many rolls' do
      params = default_params.merge(rolls: [4, 4, 2], index: 9)
      frame = Frame.new(params)

      expect(frame).not_to be_valid
    end

    it 'returns false when regular last frame has too many rolls' do
      params = default_params.merge(rolls: [4, 4, 1], index: 10)
      frame = Frame.new(params)

      expect(frame).not_to be_valid
    end

    it 'returns false when strike last frame has too many rolls' do
      params = default_params.merge(rolls: [4, 4, 1, 1], index: 10)
      frame = Frame.new(params)

      expect(frame).not_to be_valid
    end

    it 'returns false when spare last frame has too many rolls' do
      params = default_params.merge(rolls: [4, 4, 1, 1], index: 10)
      frame = Frame.new(params)

      expect(frame).not_to be_valid
    end

    it 'returns true otherwise' do
      params = default_params.merge(rolls: [6, 2])
      frame = Frame.new(params)

      expect(frame).to be_valid
    end
  end
end
